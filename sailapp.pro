# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TARGET = harbour-sailapp

CONFIG += sailfishapp
DEFINES+= VERSION=\\\"$$VERSION\\\"

SOURCES += src/harbour-sailapp.cpp \
    src/appinfo.cpp \
    src/sailapp.cpp

OTHER_FILES += qml/harbour-sailapp.qml \
    qml/cover/CoverPage.qml \
    qml/pages/FirstPage.qml \
    rpm/harbour-sailapp.changes.in \
    rpm/harbour-sailapp.spec \
    rpm/harbour-sailapp.yaml \
    translations/*.ts \
    harbour-sailapp.desktop \
    qml/pages/AboutPage.qml \
    qml/pages/LicensePage.qml \
    qml/pages/CreditsModel.qml \
    qml/pages/ChangeLog.qml

# to disable building translations every time, comment out the
# following CONFIG line
CONFIG += sailfishapp_i18n
TRANSLATIONS += translations/harbour-sailapp-de.ts

HEADERS += \
    src/gen_config.h \
    src/appinfo.h

other.files = ChangeLog.txt LICENSE.txt
other.path = /usr/share/harbour-mymoney/
INSTALLS += other
