#!/bin/bash

set -e
if [ "${1}" == "" ]; then
	echo "give me a projectname"
	exit -1
fi

PROJDIR=../"${1}"
NAME=${1}
HNAME=harbour-$NAME
if [ -e $PROJDIR ]; then
	echo "Project "$PROJDIR" already exist"
	exit -1
fi

mkdir $PROJDIR  || exit $?
mkdir -p $PROJDIR/qml/ || exit $?
mkdir $PROJDIR/src || exit $?
mkdir $PROJDIR/rpm || exit $?

cp LICENSE.txt ChangeLog.txt $PROJDIR
cp qml/sailapp.qml ${PROJDIR}/qml/${NAME}.qml
cp -r qml/cover ${PROJDIR}/qml/
cp -r qml/pages ${PROJDIR}/qml/
cp -r src/appinfo* ${PROJDIR}/src/
cp -r images/ ${PROJDIR}/
cp src/sailapp.cpp ${PROJDIR}/src/${NAME}.cpp
cp harbour-sailapp.png ${PROJDIR}/${HNAME}.png
cp harbour-sailapp.png ${PROJDIR}/${NAME}.png

VER=`cat version`
echo $VER
cat <<EOF >${PROJDIR}/${HNAME}.pro
# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TARGET = ${HNAME}

CONFIG += sailfishapp
DEFINES+= VERSION=$VER

SOURCES += src/${NAME}.cpp \\
    src/appinfo.cpp

RESOURCES += \\
    qrc.qrc


OTHER_FILES += qml/harbour-sailapp.qml \\
    qml/cover/CoverPage.qml \\
    qml/pages/FirstPage.qml \\
    qrc.qrc \\
    rpm/${HNAME}.changes.in \\
    rpm/${HNAME}.spec \\
    rpm/${HNAME}.yaml \\
    images/PayBtn_BgImg.png \\
    translations/*.ts \\
    ${HNAME}.desktop \\
    ${HNAME}.png \\
    ${NAME}.png \\
    qml/pages/AboutPage.qml \\
    qml/pages/LicensePage.qml \\
    qml/pages/CreditsModel.qml \\
    qml/pages/ChangeLog.qml

# to disable building translations every time, comment out the
# following CONFIG line
CONFIG += sailfishapp_i18n
TRANSLATIONS += translations/${HNAME}-sv.ts

HEADERS += \\
    src/gen_config.h \\
    src/appinfo.h

other.files = ChangeLog.txt LICENSE.txt
other.path = /usr/share/${HNAME}/
INSTALLS += other
EOF

cat <<EOF >$PROJDIR/src/gen_config.h
#ifndef GEN_CONFIG_H
#define GEN_CONFIG_H
#define APP_NAME "${NAME}"
#define LICENSE_TITLE "GPLv2.0"
#endif // GEN_CONFIG_H
EOF

cat <<EOF >${PROJDIR}/${HNAME}.desktop
[Desktop Entry]
Type=Application
X-Nemo-Application-Type=silica-qt5
Icon=${HNAME}
Exec=${HNAME}
Name=${HNAME}
Name[sv]=${HNAME}
EOF

cat <<EOF >${PROJDIR}/rpm/${HNAME}.yaml
Name: ${HNAME}
Summary: My SailfishOS Application
Version: 0.0.1
Release: 1
# The contents of the Group field should be one of the groups listed here:
# http://gitorious.org/meego-developer-tools/spectacle/blobs/master/data/GROUPS
Group: Qt/Qt
URL: http://example.org/
License: LICENSE
# This must be generated before uploading a package to a remote build service.
# Usually this line does not need to be modified.
Sources:
- '%{name}-%{version}.tar.bz2'
Description: |
  Short description of my SailfishOS Application
Configure: none
# The qtc5 builder inserts macros to allow QtCreator to have fine
# control over qmake/make execution
Builder: qtc5

# This section specifies build dependencies that are resolved using pkgconfig.
# This is the preferred way of specifying build dependencies for your package.
PkgConfigBR:
  - sailfishapp >= 1.0.2
  - Qt5Core
  - Qt5Qml
  - Qt5Quick

QMakeOptions:
    - VERSION='%{version}'

# Build dependencies without a pkgconfig setup can be listed here
# PkgBR:
#   - package-needed-to-build

# Runtime dependencies which are not automatically detected
Requires:
  - sailfishsilica-qt5 >= 0.10.9 

# All installed files
Files:
  - '%{_bindir}'
  - '%{_datadir}/%{name}'
  - '%{_datadir}/applications/%{name}.desktop'
  - '%{_datadir}/icons/hicolor/86x86/apps/%{name}.png'

# For more information about yaml and what's supported in Sailfish OS
# build system, please see https://wiki.merproject.org/wiki/Spectacle
EOF

cd ${PROJDIR} ; git init ; git add . ; git commit -am "Initial commit of "${NAME} 
echo "Project "$NAME" created in "$PROJDIR

cat <<EOF >${PROJDIR}/qrc.qrc
<RCC>
    <qresource prefix="/">
        <file>${HNAME}.png</file>
        <file>${NAME}.png</file>
        <file>images/PayBtn_BgImg.png</file>
    </qresource>
</RCC>
EOF

